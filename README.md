# Algorytm transpozycji macierzy wykorzystujący obliczenia równoległe

Praca zaliczeniowa z przedmiotu: Teoria Przestrzeni Danych i Algorytmów

Celem pracy zaliczeniowej jest opracowanie koncepcji oraz implementacja algorytmu transpozycji
macierzy wykorzystującej obliczenia równoległe. Macierze w systemach komputerowych mogą być
przechowywane na kilka sposobów. Jednym z nich jest zapis wierszami lub kolumnami w postaci
jednowymiarowej tablicy. Operacja transpozycji macierzy polega na przestawieniu elementów
macierzy tak, aby wiersze starej macierzy tworzyły kolumny nowej i odwrotnie. W przypadku
macierzy o znacznej liczbie elementów wykonanie jej transpozycji, nawet w realizacji równoległej,
może stanowić wyzwanie. W ramach pracy należy uwzględnić m.in. optymalizację wykorzystania
elementów roboczych oraz pamięci podręcznej procesora.