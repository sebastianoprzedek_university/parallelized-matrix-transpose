#include <cooperative_groups.h>
#include <helper_string.h>
#include <helper_image.h>
#include <helper_cuda.h>
#include <cstdio>

#define TILE_DIM    2
#define BLOCK_ROWS  2
int MATRIX_SIZE_X = 4;
int MATRIX_SIZE_Y = 8;

namespace cg = cooperative_groups;

__global__ void transpose(float *output, float *input, int width, int height)
{
    int xIndex = blockIdx.x * TILE_DIM + threadIdx.x;
    int yIndex = blockIdx.y * TILE_DIM + threadIdx.y;
    int index_in  = xIndex + width * yIndex;
    int index_out = yIndex + height * xIndex;
    for (int i=0; i<TILE_DIM; i+=BLOCK_ROWS)
		output[index_out+i] = input[index_in+i*width];
}

void computeTransposeGold(float *gold, float *input, const  int size_x, const  int size_y) {
	for (int y = 0; y < size_y; ++y)
		for (int x = 0; x < size_x; ++x) {
			gold[(x * size_y) + y] = input[(y * size_x) + x];
		}
}

int main(int argc, char **argv)
{
	int devID = 0;
    cudaDeviceProp deviceProp;
    checkCudaErrors(cudaGetDevice(&devID));
    checkCudaErrors(cudaGetDeviceProperties(&deviceProp, devID));
	
	int size_x = MATRIX_SIZE_X;
	int size_y = MATRIX_SIZE_Y;
    printf("%s has %d MP(s) x %d (Cores/MP) = %d (Cores)", deviceProp.name, deviceProp.multiProcessorCount, _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor), _ConvertSMVer2Cores(deviceProp.major, deviceProp.minor) * deviceProp.multiProcessorCount);
    
    dim3 grid(size_x/TILE_DIM, size_y/TILE_DIM), threads(TILE_DIM,BLOCK_ROWS); // execution configuration parameters
	if (grid.x < 1 || grid.y < 1) {
        printf("Transpose grid size computation incorrect in test \nExiting...\n\n");
        exit(1);
    }
    size_t mem_size = static_cast<size_t>(sizeof(float) * size_x*size_y);
    if (2*mem_size > deviceProp.totalGlobalMem) {
        printf("Input matrix size is larger than the available device memory!\n");
        printf("Please choose a smaller size matrix\n");
        exit(1);
    }    
    float *input = (float *) malloc(mem_size);
    float *output = (float *) malloc(mem_size);
    float *transposeGold = (float *) malloc(mem_size);
    float *gold, *cuda_input, *cuda_output;
    checkCudaErrors(cudaMalloc((void **) &cuda_input, mem_size));
    checkCudaErrors(cudaMalloc((void **) &cuda_output, mem_size));

	for (int i = 0; i < (size_x*size_y); ++i) {
		input[i] = (float)i;
		//printf("%4.1f ", (float)i);
	}

    checkCudaErrors(cudaMemcpy(cuda_input, input, mem_size, cudaMemcpyHostToDevice));  
    computeTransposeGold(transposeGold, input, size_x, size_y);

    printf("\nMatrix size: %dx%d (%dx%d tiles), tile size: %dx%d, block size: %dx%d\n\n", size_x, size_y, size_x/TILE_DIM, size_y/TILE_DIM, TILE_DIM, TILE_DIM, TILE_DIM, BLOCK_ROWS);
    
    gold = transposeGold;
    checkCudaErrors(cudaGetLastError());        
	transpose <<<grid, threads>>>(cuda_output, cuda_input, size_x, size_y);
    checkCudaErrors(cudaGetLastError());
    checkCudaErrors(cudaMemcpy(output, cuda_output, mem_size, cudaMemcpyDeviceToHost));
	//for (int i = 0; i < (size_x*size_y); ++i)
		//printf("%4.1f ", output[i]);
    bool res = compareData(gold, output, size_x*size_y, 0.01f, 0.0f);
    free(input);
    free(output);
    free(transposeGold);
    cudaFree(cuda_input);
    cudaFree(cuda_output);
    if (!res)
        printf("Test failed!\n");
	else
		printf("Test passed\n");
	getchar();
    exit(0);
}
